#!/bin/bash

curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo apt-get install git

mkdir git && mkdir git/server
cd git/server
git init
git remote add origin https://bitbucket.org/cfeval/demo-server.git
git pull origin master

npm install

sudo npm install -g forever

forever start source/server.js
