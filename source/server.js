const express = require("express");
const bodyParser = require("body-parser");
const os = require("os");
const products = require("./products");

const app = new express();
app.use(bodyParser.json());

const PORT = process.env.PORT || 8080;

app.get("/", (req, res) => {
   res.write(`I'm serving this from ${os.hostname()}`);
   res.end();
});

app.get("/products", products.index);
app.get("/products/:id", products.get);
app.post("/products", products.post);

app.listen(PORT);
console.log(`Listening on port ${PORT}`);