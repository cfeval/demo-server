const products = [{
    productId: "1234",
    name: "Default product",
    details: "This product is the default one. It does not have any details."
}];

const validateProduct = (product) => {
  if (product.productId === undefined) {
      throw Error("product.productId is not defined");
  }
  if (product.name === undefined) {
      throw Error("product.name is not defined");
  }
};

const addProduct = (product) => {
    validateProduct(product);
    products.push(product);
};

const getProduct = (productId) => {
    const product = products.find((product) => product.productId === productId);
    return product;
};

const postProductEndpoint = (req, res) => {
    const product = req.body;
    try {
        addProduct(product);
    } catch (error) {
        res.status(400);
        res.write(error.message);
    }
    res.end();
};

const indexProductEndpoint = (req, res) => {
    res.write(JSON.stringify(products));
    res.end();
};

const getProductEndpoint = (req, res) => {
    const productId = req.params.id;
    const product = getProduct(productId);
    if (product === undefined) {
        res.status(404);
        res.write((JSON.stringify({
            error: "Product not found"
        })));
    } else {
        res.write(JSON.stringify(product));
    }
    res.end();
};

module.exports = {
    post: postProductEndpoint,
    index: indexProductEndpoint,
    get: getProductEndpoint
};