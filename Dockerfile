FROM node:8.7.0
WORKDIR /usr/src/app
COPY package.json package-lock.json  ./
RUN npm install
COPY source/ source/
EXPOSE 8080
CMD [ "npm", "start" ]